//
//  Burger.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import Foundation

struct Movie: Decodable{
    var name: String
    var cast: String
    var imageName: String
    var thumbnailName: String
    var type: MovieType
}

enum MovieType: String, Decodable{
    case comedy = "comedy"
    case action = "action"
    case scifi = "scifi"
}
