//
//  BurgerModel.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import Foundation

class MovieModel {
    private (set) var movies: [Movie] = []
    
    init(){
        if let url = Bundle.main.url(forResource: "MovieResources/movie", withExtension: "json"){
            do{
                let data = try Data(contentsOf: url)
                movies = try JSONDecoder().decode([Movie].self, from: data)
            }
            catch{
                print(error)
            }
        }
         
    }
    
    func movies(forType type: MovieType?) -> [Movie]{
        guard let type = type else {return movies}
        return movies.filter {$0.type == type}
    }
    
    func add(movie: Movie){
        movies.append(movie)
    }
    
}
